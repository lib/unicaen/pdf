Bibliothèque unicaen/pdf
========================

Export PDF
----------

La classe `\UnicaenPdf\Exporter\PdfExporter` facilite la fabrication d'un
document PDF et voici ses arguments principaux :

-   Nourrissage à partir de marquage HTML, spécifié directement sous 
    forme de chaînes de caractères ou bien par l'intermédiaire d'un script de vue.
-   Format A4, A3, etc., portrait ou paysage.
-   En-tête contenant le logo de l'UCBN (spécifié en dur, désolé!), un 
    titre éventuel et un sous-titre éventuel. Possibilité de 
    différencier en-tête des pages paires et en-tête des pages impaires.
-   Pied de page contenant l'heure de génération du document, le numéro
    de page, le nombre total de pages et un titre éventuel. Possibilité
    de différencier pied des pages paires et pied des pages impaires.
-   Un texte en filigrane éventuel (watermark).
-   Spécification possible des opérations autorisées sur le document
    généré et le mot de passe éventuel permettant d'ouvrir le document.

*NB : C'est la bibliothèque mPDF (https://mpdf.github.io/) qui motorise la génération PDF.*

Exemple d'utilisation :

```php
public function pdfAction()
{
    /* @var $renderer \Laminas\View\Renderer\PhpRenderer */
    $renderer = $this->getServiceLocator()->get('view_renderer'); 
    $exporter = new \UnicaenPdf\Exporter\PdfExporter($renderer, 'A4', true);
    $exporter->addBodyHtml('<style>' . file_get_contents($cssFilePath) . '</style>');
    $exporter->addBodyHtml('<h1>Module UnicaenPdf</h1>');
    $exporter->addBodyHtml('<h2>Export PDF</h2>', false);
    $exporter->addBodyScript('application/demo/paragraphe.phtml', false);
    $exporter->addBodyScript('application/demo/nouvelle-page.phtml', true);
    $exporter->export('export.pdf');
}
```
