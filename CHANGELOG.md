CHANGELOG
=========

6.1.0
-----
- Ajout des commandes shell de concaténation de fichiers PDF avec Ghostscript ou QPDF.

6.0.1
-----
- Ajout d'une fonction statique afin de simplifier le code de création de PDF dans les applications. 

6.0.0
-----
- Version PHP 8.

5.0.2
-----
- Ajout d'une fonction statique afin de simplifier le code de création de PDF dans les applications.

5.0.1
-----
- Reprise manuelle des dernières évolutions de l'Exporter dans unicaen/app

5.0.0
-----
- Possibilité de passer à PHP 8.

4.0.0
-----
- Migration vers Laminas
- Correctif sur la possibilité d'exporter le pdf dans un repertoire autre que /tmp
- Extraction de ce module d'unicaen/app.
