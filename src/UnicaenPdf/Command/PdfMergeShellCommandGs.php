<?php

namespace UnicaenPdf\Command;

use UnicaenPdf\Command\PdfMergeShellCommandInterface;
use UnicaenShell\Command\ShellCommand;

/**
 * Commande de concaténation de N fichiers PDF.
 *
 * Version utilisant 'ghostscript'.
 */
final class PdfMergeShellCommandGs extends ShellCommand implements PdfMergeShellCommandInterface
{
    protected string $noCompressionOption = '-dColorConversionStrategy=/LeaveColorUnchanged -dDownsampleMonoImages=false -dDownsampleGrayImages=false -dDownsampleColorImages=false -dAutoFilterColorImages=false -dAutoFilterGrayImages=false -dColorImageFilter=/FlateEncode -dGrayImageFilter=/FlateEncode';

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'PdfMergeShellCommandGs';
    }

    public function checkRequirements(): void
    {
        $this->assertExecutableExists();
    }

    /**
     * @inheritDoc
     */
    public function generateCommandLine()
    {
        $command = $this->executable . ' ' . $this->noCompressionOption;
        $command .=
            ' -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=' . $this->outputFilePath .
            ' -dBATCH ' . implode(' ', $this->inputFilesPaths);

        $this->commandLine = $command;
    }
}