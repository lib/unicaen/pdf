<?php

namespace UnicaenPdf\Command;

use UnicaenShell\Command\ShellCommandInterface;

/**
 * Commande de concaténation de fichiers PDF.
 */
interface PdfMergeShellCommandInterface extends ShellCommandInterface
{
}