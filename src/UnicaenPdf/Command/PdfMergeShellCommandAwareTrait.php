<?php

namespace UnicaenPdf\Command;

trait PdfMergeShellCommandAwareTrait
{
    protected PdfMergeShellCommandInterface $pdfMergeShellCommand;

    public function setPdfMergeShellCommand(PdfMergeShellCommandInterface $pdfMergeShellCommand): void
    {
        $this->pdfMergeShellCommand = $pdfMergeShellCommand;
    }
}