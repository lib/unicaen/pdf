<?php

namespace UnicaenPdf\Command;

use UnicaenPdf\Command\PdfMergeShellCommandInterface;
use UnicaenShell\Command\ShellCommand;

/**
 * Commande de concaténation de N fichiers PDF.
 *
 * Version utilisant 'qpdf'.
 */
final class PdfMergeShellCommandQpdf extends ShellCommand implements PdfMergeShellCommandInterface
{
    public function getName(): string
    {
        return 'MergeShellCommandQpdf';
    }

    public function checkRequirements(): void
    {
        $this->assertExecutableExists();
    }

    public function generateCommandLine()
    {
        // Commande de fusion (cf. https://qpdf.readthedocs.io/en/10.5/cli.html#page-selection-options)
        $command = $this->executable .
            sprintf(' --warning-exit-0 %s --pages . %s -- %s',
                array_shift($this->inputFilesPaths),
                implode(' ', $this->inputFilesPaths),
                $this->outputFilePath
            );

        $this->commandLine = $command;
    }
}