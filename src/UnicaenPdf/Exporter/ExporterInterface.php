<?php

namespace UnicaenPdf\Exporter;

interface ExporterInterface
{
    /**
     * @return \Laminas\View\Renderer\PhpRenderer
     */
    public function getRenderer(): \Laminas\View\Renderer\PhpRenderer;
    
    /**
     * @param string|null $filename
     */
    public function export(string $filename = null);
}