<?php

namespace UnicaenPdf\Options;

use Interop\Container\ContainerInterface;

/**
 * @author Unicaen
 */
class ModuleOptionsFactory
{
    public function __invoke(ContainerInterface $container): ModuleOptions
    {
        return new ModuleOptions();
    }
}