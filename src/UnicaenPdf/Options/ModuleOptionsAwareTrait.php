<?php

namespace UnicaenPdf\Options;

trait ModuleOptionsAwareTrait
{
    protected ModuleOptions $moduleOptions;

    public function setModuleOptions(ModuleOptions $moduleOptions): void
    {
        $this->moduleOptions = $moduleOptions;
    }
}