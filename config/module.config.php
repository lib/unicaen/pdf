<?php

namespace UnicaenPdf;

use UnicaenPdf\Command\PdfMergeShellCommandGs;
use UnicaenPdf\Command\PdfMergeShellCommandQpdf;
use UnicaenPdf\Options\ModuleOptions;
use UnicaenPdf\Options\ModuleOptionsFactory;

return [
    'unicaen-pdf' => [

    ],

    'unicaen-shell' => [
        'commands' => [
            PdfMergeShellCommandGs::class => [
                'executable' => '/usr/bin/gs',
            ],
            PdfMergeShellCommandQpdf::class => [
                'executable' => '/usr/bin/qpdf',
            ],
        ],
    ],

    'router' => [
        'routes' => [
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            ModuleOptions::class => ModuleOptionsFactory::class,
        ],
    ],
    'view_helpers' => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],
    'controllers' => [
        'factories' => [
        ],
    ],
    'navigation' => [
        'default' => [
        ],
    ],
    'bjyauthorize' => [
        'guards' => [
        ],
    ],
];
